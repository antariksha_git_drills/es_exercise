function letUse(){
    let a=1;
    let b=1;
    return a+b;
}
console.log(letUse());
function letConst(){
    const a=1;
    const b=2;
    const c=3;
    try{
        b=a+c;
        return b;
    }catch(err){
        console.log('Constant variable is not mutable');
        return b;
    }
}
console.log(letConst());
let square=(x) =>{
    return x*x; 
}
console.log(square(4));
let add=(a,b) =>{
    return a+b;
}
console.log(add(4,5));
let lines='Hello, my name is A. I am XX years old. I live in B. I was born on DD MM YYYY. My favourite hobby is C';
let splittedLines=lines.split('.');
for(let line of splittedLines){
    console.log(line);
}
function circleRadius(rad){
    if(rad===undefined){
        rad=5;
    }
    return 3.14*rad*rad;
}
console.log(circleRadius());
let person = {
    name: 'Harry Potter',
    location: 'London'
}
let text=`${person.name} is located in ${person.location}`;
console.log(text);
let [a,b,...c]=['India','Russia','Austria','Brazil'];
console.log(a+' '+b);
function destruct([a,...b]){
    console.log(b);
}
destruct(['India','Russia','Austria','Brazil']);
function intro({Firstname,Surname}){
    console.log(`My name is ${Firstname} ${Surname}`);
}
const name={Firstname:'Abraham',Surname:'Van Helsing'};
intro(name);
let fruit='banana';
let vegetable='spinach';
let meat='pork';
let food={fruit,vegetable,meat};
console.log(food);
function sum(...num){
    let add=0;
    for(val of num){
        add+=val;
    }
    return add;
}
console.log(sum(...[1,2,3,4,5,6,7]));
let forLoop=[1,2,3,4,5,6,7,8,9,10];
for(let val of forLoop){
    console.log(val);
}
let objectLoop={Name:'A',Age:'1',Location:'B',Experience:'C'};
console.log(Object.keys(objectLoop));
console.log(Object.values(objectLoop));
for(let [key,value] of Object.entries(objectLoop)){
    console.log(key+' '+value);
}